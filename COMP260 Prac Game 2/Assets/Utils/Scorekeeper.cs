﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour {

	// Use this for initialization
	static private Scorekeeper instance;
	static public Scorekeeper Instance {
		get { return instance; }
	}
	public int pointsPerGoal = 1;
	private int[] score = new int[2];
	public Text[] scoreText;
	public Text Victory;
	void Start () {
		if (instance == null) {
			// save this instance
			instance = this;
		} else {
			// more than one instance exists
			Debug.LogError (
				"More than one Scorekeeper exists in the scene.");

			// reset the scores to zero
			for (int i = 0; i < score.Length; i++) {
				score [i] = 9;
				scoreText [i].text = "9";
			}
		}
	}

	public void OnScoreGoal(int player) {
		score[player] += pointsPerGoal;
		scoreText[player].text = score[player].ToString ();

		if (score [1] == 10) {
			StartCoroutine (aiwin());
		}
		if (score [0] == 10) {
			StartCoroutine (playerwin());
		}
	}

	IEnumerator aiwin(){
		Victory.text = "AI Wins!";
		yield return new WaitForSeconds (10);
		Application.Quit ();
	}

	IEnumerator playerwin(){
		Victory.text = "Player Wins!";
		yield return new WaitForSeconds (10);
		Application.Quit ();
	}
}