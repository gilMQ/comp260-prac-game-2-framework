﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PuckControl : MonoBehaviour {
	void OnCollisionEnter(Collision collision) {
		Debug.Log("Collision Enter: " + collision.gameObject.name);

	}

	void OnCollisionStay(Collision collision) {
		Debug.Log("Collision Stay: " + collision.gameObject.name);
	}

	void OnCollisionExit(Collision collision) {
		Debug.Log("Collision Exit: " + collision.gameObject.name);

	}
	public Transform startingPos;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		ResetPosition();

	}

	public void ResetPosition() {
		// teleport to the starting position
		rigidbody.MovePosition(startingPos.position);
		// stop it from moving
		rigidbody.velocity = Vector3.zero;

	}

	// Update is called once per frame
	void Update () {
		
	}
}
