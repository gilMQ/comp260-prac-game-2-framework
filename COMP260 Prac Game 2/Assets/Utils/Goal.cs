﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {
		public AudioClip scoreClip;
		public int player;
//		private AudioSource audio;

		void Start() {
//			audio = GetComponent<AudioSource> ();
		}

		void OnTriggerEnter(Collider collider) {
			// play score sound
//			audio.PlayOneShot(scoreClip);
		// tell the scorekeeper
		Scorekeeper.Instance.OnScoreGoal(player);

		//resets puck
		PuckControl puck =
			collider.gameObject.GetComponent<PuckControl> ();
		puck.ResetPosition ();
	}
	// Update is called once per frame
	void Update () {
		
	}
}
