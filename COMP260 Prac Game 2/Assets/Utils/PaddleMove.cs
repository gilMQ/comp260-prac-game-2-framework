﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PaddleMove : MonoBehaviour {
	public float speed = 5f;
	public float moveSpeed = 5f;
	//public Velocity
	//public float force = 10f;
	// Use this for initialization
	private Rigidbody rigidbody;
	private Vector3 GetMousePosition() {
		// create a ray from the camera 
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// find out where the ray intersects the XZ plane
		Plane plane = new Plane(Vector3.up, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	} 

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;

	}
		
	// Update is called once per frame
	void Update () {
	/*	Debug.Log("Time =" + Time.time);
		Vector3 direction;
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");
		// move the object
		/*	transform.Translate(velocity * Time.deltaTime); 
		var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
		var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f; */

	/*	transform.Rotate(0, x, 0);
		transform.Translate(0, 0, z); */
	}

	void FixedUpdate () {
	/*	Debug.Log("Fixed Time =" + Time.fixedTime);
		Vector3 pos = GetMousePosition();
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;

		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;*/

		float moveH = moveSpeed * Input.GetAxis ("Horizontal");
		float moveV = moveSpeed * Input.GetAxis ("Vertical");

		Vector3 dir = ((moveH * Vector3.right) + (moveV * Vector3.forward)) * speed;
		Vector3 vel = dir.normalized * speed;
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;
		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;
	
		/* Vector3 pos = GetMousePosition();
		Vector3 dir = pos - rigidbody.position;

		rigidbody.AddForce(dir.normalized * force); */
	}

	/*void OnDrawGizmos() {
		// draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	} */

}